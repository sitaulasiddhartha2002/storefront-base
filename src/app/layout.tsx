import Providers from "@modules/providers"
import "styles/globals.css"
import {NextUIProvider} from "@nextui-org/react";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en" data-mode="light">
      <body className="w-[100vw]">
        <Providers>
          <NextUIProvider>
          <main className="relative">{children}</main>
          </NextUIProvider>
        </Providers>
      </body>
    </html>
  )
}
