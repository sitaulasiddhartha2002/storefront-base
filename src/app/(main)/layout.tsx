import Footer from "@modules/layout/templates/footer"
import Nav from "@modules/layout/templates/nav"
import {Tabs, Tab} from "@nextui-org/react";




export default function PageLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <Nav />
      {children}
      <Footer />
    </>
  )
}
