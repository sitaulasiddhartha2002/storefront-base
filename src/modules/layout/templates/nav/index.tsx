"use client"

import { useMobileMenu } from "@lib/context/mobile-menu-context"
import useToggleState from "@lib/hooks/use-toggle-state"
import Hamburger from "@modules/common/components/hamburger"
import CartDropdown from "@modules/layout/components/cart-dropdown"
import SideMenu from "@modules/layout/components/side-menu"
import MobileMenu from "@modules/mobile-menu/templates"
import DesktopSearchModal from "@modules/search/templates/desktop-search-modal"
import Link from "next/link"
import {Avatar} from "@nextui-org/react";
import { Button } from "@nextui-org/react";
import React from "react"
import { useProductCategories } from "medusa-react"
import { ProductCategory } from "@medusajs/medusa"





const Nav = () => {
  const { 
    product_categories, 
    
  } = useProductCategories()
  const { toggle } = useMobileMenu()
  const {
    state: searchModalState,
    close: searchModalClose,
    open: searchModalOpen,
  } = useToggleState()

  return (
    <div className="sticky top-0 inset-x-0 z-50 group md:mb-32 lg:mb-24">
      <header className="relative h-16 px-8 mx-auto border-b duration-200 bg-white border-ui-border-base">
        <nav className="txt-xsmall-plus text-ui-fg-subtle flex items-center justify-between w-full h-full text-small-regular">
          <div className="flex-1 basis-0 h-full flex items-center">
            <div className="block small:hidden">
              <Hamburger setOpen={toggle} />
            </div>
            <div className="hidden small:block h-full">
              <SideMenu searchModalOpen={searchModalOpen} />
            </div>
          </div>

          <div className="flex items-center h-full">
            <Link
              href="/"
              
            >
              <Avatar src="/logo.png" alt="Vastra Designers Logo"  size="lg"/>
            </Link>
          </div>

          <div className="flex items-center gap-x-6 h-full flex-1 basis-0 justify-end">
            <div className="hidden small:flex items-center gap-x-6 h-full">
              {process.env.FEATURE_SEARCH_ENABLED && (
                <DesktopSearchModal
                  state={searchModalState}
                  close={searchModalClose}
                  open={searchModalOpen}
                />
              )}
            </div>
            <CartDropdown />
              <Link className="hover:text-ui-fg-base" href="/account">
              <Avatar isBordered src="https://img.icons8.com/3d-fluency/94/guest-male--v2.png" alt="guest-male--v2"  size="sm"/>
              </Link>
          </div>
        </nav>

        <MobileMenu />

        <div className="flex flex-row justify-around pt-1 bg-white invisible md:visible w-[100vw]">
        {product_categories && product_categories.length > 0 && (
        <ul>
          {product_categories.map(
            (category: ProductCategory) => (
              <Button
              href={`/${category.handle}`}
              as={Link}
              key={category.id}
              radius="full"
              variant="flat"
              className="m-2"
            >
              <Link href={`/${category.handle}`}>{category.name}</Link>
            </Button>
        
            )
          )}
        </ul>
      )}

        

        </div>
    
      </header>
    </div>
  )
}

export default Nav
