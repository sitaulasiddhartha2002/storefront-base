import React from "react";
export const HamburgerCustom = ({ size=6, height=6, width=6, ...props }:{size?:number,height?:number,width?:number}) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className={`w-${width} h-${height}`}>
  <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
</svg>

  );
};
