"use client"

import clsx from "clsx"
import { useCollections, useProductCategories } from "medusa-react"
import { Text } from "@medusajs/ui"
import Link from "next/link"
import { FaFacebook,FaInstagram } from "react-icons/fa6";

const FooterNav = () => {
  const { collections } = useCollections()
  const { product_categories } = useProductCategories()

  return (
    <div className="border-t border-ui-border-base w-screen">

      <div className="content-container flex flex-col">
        <div className="flex flex-col gap-y-6 xsmall:flex-row items-start justify-between py-40">
          <div>
            <Link
              href="/"
              className="txt-compact-xlarge-plus text-ui-fg-subtle hover:text-ui-fg-base uppercase"
            >
              Vastra Designers
            </Link>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3314.668662375865!2d151.009409!3d-33.8208627!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a32307b8b571%3A0x5b420697a52ace23!2s83%20Wigram%20St%2C%20Harris%20Park%20NSW%202150%2C%20Australia!5e0!3m2!1sen!2snp!4v1705555816811!5m2!1sen!2snp" className="md:w-[650px]" style={{
        margin:'auto',
        borderRadius:'5px'
      }} height="450" loading="lazy"></iframe>

          </div>
          <div className="text-small-regular grid grid-cols-3 gap-x-10 md:gap-x-16">
            {product_categories && (
              <div className="flex flex-col gap-y-2">
                <span className="txt-small-plus txt-ui-fg-base">
                  Categories
                </span>
                <ul className="grid grid-cols-1 gap-2">
                  {product_categories?.slice(0, 6).map((c) => {
                    if (c.parent_category) {
                      return
                    }

                    const children =
                      c.category_children?.map((child) => ({
                        name: child.name,
                        handle: child.handle,
                        id: child.id,
                      })) || null

                    return (
                      <li
                        className="flex flex-col gap-2 text-ui-fg-subtle txt-small"
                        key={c.id}
                      >
                        <Link
                          className={clsx(
                            "hover:text-ui-fg-base",
                            children && "txt-small-plus"
                          )}
                          href={`/${c.handle}`}
                        >
                          {c.name}
                        </Link>
                        {children && (
                          <ul className="grid grid-cols-1 ml-3 gap-2">
                            {children &&
                              children.map((child) => (
                                <li key={child.id}>
                                  <Link
                                    className="hover:text-ui-fg-base"
                                    href={`/${child.handle}`}
                                  >
                                    {child.name}
                                  </Link>
                                </li>
                              ))}
                          </ul>
                        )}
                      </li>
                    )
                  })}
                </ul>
              </div>
            )}
            {collections && (
              <div className="flex flex-col gap-y-2">
                <span className="txt-small-plus txt-ui-fg-base">
                  Collections
                </span>
                <ul
                  className={clsx(
                    "grid grid-cols-1 gap-2 text-ui-fg-subtle txt-small",
                    {
                      "grid-cols-2": (collections?.length || 0) > 3,
                    }
                  )}
                >
                  {collections?.slice(0, 6).map((c) => (
                    <li key={c.id}>
                      <Link
                        className="hover:text-ui-fg-base"
                        href={`/collections/${c.handle}`}
                      >
                        {c.title}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            )}
            <div className="flex flex-col gap-y-2">
              <span className="txt-small-plus txt-ui-fg-base">Socials</span>
              <ul className="grid grid-cols-1 gap-y-2 text-ui-fg-subtle txt-small">
                <li>
                  <a
                    href="https://www.facebook.com/vastradesignerssydney"
                    target="_new"
                    rel="noreferrer"
                    className="hover:text-ui-fg-base"
                  >
                    <span className="flex flex-row items-center">
                      <FaFacebook size={15}/> <span className="px-1">Facebook</span>
                    </span>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/vastradesigners_sydney/"
                    target="_new"
                    rel="noreferrer"
                    className="hover:text-ui-fg-base"
                  >
                    <span className="flex flex-row items-center">
                      <FaInstagram size={15}/> <span className="px-1">Instagram</span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="flex w-full mb-16 justify-between text-ui-fg-muted">
          <Text className="txt-compact-small">
            © {new Date().getFullYear()} Vastra Deginers. All rights reserved.
          </Text>
          {/* <MedusaCTA /> */}
        </div>
      </div>
    </div>
  )
}

export default FooterNav
