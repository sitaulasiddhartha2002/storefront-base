import {Card, CardHeader, CardFooter, Image, Button} from "@nextui-org/react";
import Link from "next/link"

const Hero = () => {
    return (
      <div className="w-[100vw] gap-2 grid grid-cols-12 grid-rows-2 px-10 mt-8 md:px-32">
      <Card className="col-span-12 md:col-span-4 h-[350px]" >
        <CardHeader className="absolute z-10 top-1 flex-col !items-start">
          <p className="text-tiny text-white/60 uppercase font-bold">New</p>
          <h4 className="text-white font-medium text-large">Ethinic Wears for Girls</h4>
        </CardHeader>
        <Image
          removeWrapper
          alt="Card background"
          className="object-center z-0 w-full h-full object-cover hover:scale-105"
          src="/girl.jpg"
        />
        <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">

          <Button className="text-tiny" color="primary" radius="full" size="sm">
          <Link rel="icon" href="/girls" >
            <>
              Shop more
            </>
          </Link>
          </Button>
        </CardFooter>
      </Card>
      <Card className="col-span-12 md:col-span-4 h-[350px]">
        <CardHeader className="absolute z-10 top-1 flex-col !items-end">
          <p className="text-tiny text-black/60 uppercase font-bold">New</p>
          <h4 className="text-black font-medium text-large">Ethinic Wears for Boys</h4>
        </CardHeader>
        <Image
          removeWrapper
          alt="Card background"
          className="object-top z-0 w-full hover:scale-105"
          src="/boy.jpg"
        />
        <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">
          <Button className="text-tiny" color="primary" radius="full" size="sm">
          <Link rel="icon" href="/boys" >
            <>
              Shop more
            </>
          </Link>
          </Button>
        </CardFooter>
      </Card>
      <Card className="col-span-12 md:col-span-4 h-[350px]">
        <CardHeader className="absolute z-10 top-1 flex-col !items-start">
          <p className="text-tiny text-white/60 uppercase font-bold">New</p>
          <h4 className="text-white font-medium text-large">Ethinic Wears for Men</h4>
        </CardHeader>
        <Image
          removeWrapper
          alt="Card background"
          className="object-top z-0 w-full h-full object-cover hover:scale-105"
          src="/men.jpg"
        />
        <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">

          <Button className="text-tiny" color="primary" radius="full" size="sm">
          <Link rel="icon" href="/men" >
            <>
              Shop more
            </>
          </Link>
          </Button>
        </CardFooter>
      </Card>
      <Card isFooterBlurred className="w-full h-[350px] col-span-12 sm:col-span-5">
        <CardHeader className="absolute z-10 top-1 flex-col items-start">
          <p className="text-tiny text-white/60 uppercase font-bold">New</p>
          <h4 className="text-white font-medium text-2xl">Ethinic Wears for Women</h4>
        </CardHeader>
        <Image
          removeWrapper
          alt="Card example background"
          className="object-top z-0 w-full h-full -translate-y-6 object-cover hover:scale-105"
          src="/women.jpg"
        />
        <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">

          <Button className="text-tiny" color="primary" radius="full" size="sm">
          <Link rel="icon" href="/women" >
            <>
              Shop more
            </>
          </Link>
          </Button>
        </CardFooter>
      </Card>
      <Card isFooterBlurred className="w-full h-[350px] col-span-12 sm:col-span-7">
        <CardHeader className="absolute z-10 top-1 flex-col items-end">
          <h4 className="text-white/90 font-medium text-xl">Indian, Nepalese, Bangladeshi and Pakistani ethnic wear</h4>
          <p className="text-tiny text-white/60 uppercase font-bold">footwear and accessories at Harris Park, SYD</p>
        </CardHeader>
        <Image
          removeWrapper
          alt="Relaxing app background"
          className="object-center z-0 w-full h-full object-cover hover:scale-105"
          src="/all.jpg"
        />
        <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">
          <Button className="text-tiny" color="primary" radius="full" size="sm">
          <Link rel="icon" href="/store" >
            <>
              Shop more
            </>
          </Link>
          </Button>
        </CardFooter>
      </Card>
    </div>
  )
}

export default Hero
